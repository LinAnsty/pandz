#ifndef CALCDIRECTION_H
#define CALCDIRECTION_H
#include "Matrix.h"

void calcDirection(TVECTORD& VelVecData, TVECTORD& HeightVecData, PLANE& plane);

#endif // CALCDIRECTION_H
