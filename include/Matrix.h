#ifndef MATRIX_H
#define MATRIX_H

#include "Calculations.h"
#include <QtCore/QFile>

typedef QVector<double> TVECTORD;
typedef QVector<QVector <int>> TMATRIXI;
typedef QVector<QVector <double>> TMATRIXD;

void CreateHorizontalMatrix(TMATRIXD& M, PLANE& plane);
void CreateVerticalMatrix(TMATRIXD& M, PLANE& plane);
void CreateVHMatrix(TMATRIXD& M, PLANE& plane);

#endif // MATRIX_H
