#ifndef CALCULATIONS_H
#define CALCULATIONS_H
#include <math.h>

typedef struct {
    //variables that doesnt change obly once at start
    double dx;
    double dy;
    double H1;
    double H2;
    double V1;
    double V2;
    double m;
    double dV;
    double dH;

    //variables that changes over time
    double V; //current speed
    double H; //current Height
} PLANE;

double calcH(double V);
double SpeedGainTime(PLANE &plane);
double ClimbTime(PLANE &plane);
double SpeedClimbTime(PLANE &plane);
#endif // CALCULATIONS_H
