#include "../include/CalcDirection.h"

/*
make 2 vectors with speed and height values
need this to create graph then
input values calculated at calcDirection step
*/
void MakeCoords(TVECTORD &vecV, TVECTORD &vecH, TMATRIXI &DirectionMatrix, PLANE &plane) {
    int i, j;
    i = DirectionMatrix.size() - 1;
    j = DirectionMatrix[i].size() - 1;

    double dv = 0.0, dh = 0.0;

    //counts how much accelerations and climbs we have, for dv and dh
    int velCounter = 0, hCounter = 0;

    while (DirectionMatrix[i][j] != 0) {
        switch (DirectionMatrix[i][j]) {
        case 1:
            velCounter++;
            j--;
            break;

        case 2:
            hCounter++;
            i--;
            break;

        case 3:
            velCounter++;
            hCounter++;
            i--;
            j--;
            break;

        default:
            break;
        }

    }

    //making correct step values to calculate speed and height
    dv = (plane.V2 - plane.V1)/velCounter;
    dh = (plane.H2 - plane.H1)/hCounter;

    //set iterator
    i = DirectionMatrix.size() - 1;
    j = DirectionMatrix[i].size() - 1;

    //set first vector value
    vecV.append(plane.V2);
    vecH.append(plane.H2);

    while (DirectionMatrix[i][j] != 0) {
        switch (DirectionMatrix[i][j]) {
        case 1:
            vecV.append(vecV.last() - dv);
            vecH.append(vecH.last());
            j--;
            break;

        case 2:            
            vecH.append(vecH.last() - dh);
            vecV.append(vecV.last());
            i--;
            break;

        case 3:
            vecV.append(vecV.last() - dv);
            vecH.append(vecH.last() - dh);
            i--;
            j--;
            break;

        default:
            break;
        }
    }
}

/*
    calculating data for output as 2 vectors of speed and height at 1 step
    direction matrix contains numbers which are representation of movement type at every step
*/
void calcDirection(TVECTORD& VelVecData, TVECTORD& HeightVecData, PLANE& plane) {

    TMATRIXD HM, VM, VHM;
    TMATRIXI D;

    //DIRECTION contains numbers of best trajectory with types of movement \
        1 is for horizontal acceleration    \
        2 is for climb without speed gain \
        3 both 1 and 2 like gaining speed and climb \

    //calculate time for climb, speed gain, and both of them
    CreateVerticalMatrix(VM, plane);
    CreateHorizontalMatrix(HM, plane);
    CreateVHMatrix(VHM, plane);


    // make border values

    //1 raw setting with 1, so if we are at the top of the matrix we need only acceleration \
    and 1 element keep staying 0
    QVector<int> raw;
    for (int j = 0; j < HM[0].size(); j++) {
        raw.append(1);
    }
    D.append(raw);

    raw.clear();

    //on left first column when j = 0 we setting with 2, so if we are at the left border \
    of the matrix we only climbing
    for (int i = 0; i < VM.size(); i++) {
        raw.append(2);
        D.append(raw);
        raw.clear();
    }

    for (int i = 0; i < VM.size(); i++) {
        for (int j = 0; j < HM[0].size(); j++) {
            if ((HM[i+1][j] + VM[i][j]) < VHM[i][j]) {
                D[i+1].append(1);
            } else if ((HM[i][j] + VM[i][j+1]) < VHM[i][j]) {
                D[i+1].append(2);
            } else
                D[i+1].append(3);
        }
    }

    D[0][0] = 0;

    //Make coordinates for graph from Direction Matrix(D)
    MakeCoords(VelVecData, HeightVecData, D, plane);
}
