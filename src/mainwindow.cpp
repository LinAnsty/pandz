#include "../include/mainwindow.h"
#include "ui_mainwindow.h"
#include <algorithm>
#include <functional>
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    //Default parameters H1{meters}   H2{meters}    V1{meters/sec}                  V2{meters/sec}                    m{kg}
    //                   400          6500          320.0{km/h} * 1000.0 / 3600.0   720.0{km/h} * 1000.0 / 3600.00    47000.0
    ui->StepslineEdit->setText("10");
    ui->V1lineEdit->setText("320");
    ui->V2lineEdit->setText("720");
    ui->H1lineEdit->setText("400");
    ui->H2lineEdit->setText("6500");
    ui->MasslineEdit->setText("47000.0");
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    PLANE tu134;

    //seting validator
    ui->H1lineEdit->setValidator(new QDoubleValidator(this));
    ui->H2lineEdit->setValidator(new QDoubleValidator(this));
    ui->V1lineEdit->setValidator(new QDoubleValidator(this));
    ui->V2lineEdit->setValidator(new QDoubleValidator(this));
    ui->MasslineEdit->setValidator(new QDoubleValidator(this));
    ui->StepslineEdit->setValidator(new QDoubleValidator(this));

    //getting input values
    tu134.dx = tu134.dy = ui->StepslineEdit->text().toDouble(); //number of axial approximations
    tu134.H1 =  ui->H1lineEdit->text().toDouble(); //min Height
    tu134.H2 = ui->H2lineEdit->text().toDouble(); //max Height;
    tu134.V1 = ui->V1lineEdit->text().toDouble() * 1000.0 / 3600.0; //min Speed
    tu134.V2 = ui->V2lineEdit->text().toDouble() * 1000.0 / 3600.0; //max Speed
    tu134.m = ui->MasslineEdit->text().toDouble(); //mass
    tu134.dV = (tu134.V2 - tu134.V1) / tu134.dx; //step between V1 and V2
    tu134.dH = (tu134.H2 - tu134.H1) / tu134.dy; //step between H1 and H2
    tu134.H = 0; //current speed at specified time
    tu134.V = 0; //current speed at specified time

    // x -  Velocity vector; y - Height vector
    QVector<double> x, y;

    calcDirection(x, y, tu134);

    // multiplying vector with 3.6 to convert from m/s to km/h
    std::transform(x.begin(), x.end(), x.begin(),
                   std::bind(std::multiplies<double>(), std::placeholders::_1, 3.6));


    //reversing x, y for better set border functons
    std::reverse(x.begin(), x.end());
    std::reverse(y.begin(), y.end());

    //clean graph
    ui->widget->clearGraphs();

    //set data, x and y are sorted from max to min;
    ui->widget->addGraph();
    ui->widget->graph(0)->setData(x, y, true);

    //set Labels for axis
    ui->widget->xAxis->setLabel("V, km/h");
    ui->widget->yAxis->setLabel("H(V), km");

    // set border range for x axis
    ui->widget->xAxis->setRange(x.first() - x.first() / tu134.dx, x.last() + x.last() / tu134.dx);
    // set border range for y axis
    ui->widget->yAxis->setRange(0, y.last() + y.last() / tu134.dx);

    //
    ui->widget->replot();

}
